/*
 * Copyright © 2012-2015, Intel Corporation. All rights reserved.
 * Please see the included README.md file for license terms and conditions.
 */


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false, app:false, dev:false */
/*global myEventHandler:false, cordova:false, device:false */



window.app = window.app || {} ;         // there should only be one of these...



// Set to "true" if you want the console.log messages to appear.

app.LOG = app.LOG || false ;

app.consoleLog = function() {           // only emits console.log messages if app.LOG != false
    if( app.LOG ) {
        var args = Array.prototype.slice.call(arguments, 0) ;
        console.log.apply(console, args) ;
    }
} ;



// App init point (runs on custom app.Ready event from init-dev.js).
// Runs after underlying device native code and webview/browser is ready.
// Where you should "kick off" your application by initializing app events, etc.

// NOTE: Customize this function to initialize your application, as needed.

app.initEvents = function() {
    "use strict" ;
    var fName = "app.initEvents():" ;
    app.consoleLog(fName, "entry") ;

    // NOTE: initialize your third-party libraries and event handlers

    // initThirdPartyLibraryNumberOne() ;
    // initThirdPartyLibraryNumberTwo() ;
    // initThirdPartyLibraryNumberEtc() ;

    // NOTE: initialize your application code

    // initMyAppCodeNumberOne() ;
    // initMyAppCodeNumberTwo() ;
    // initMyAppCodeNumberEtc() ;

    // NOTE: initialize your app event handlers, see app.js for a simple event handler example

    // TODO: configure following to work with both touch and click events (mouse + touch)
    // see http://msopentech.com/blog/2013/09/16/add-pinch-pointer-events-apache-cordova-phonegap-app/

//...overly simple example...
    var el, evt ;

    if( navigator.msPointerEnabled || !('ontouchend' in window))    // if on Win 8 machine or no touch
       evt = "click" ;                                             // let touch become a click event
    else                                                            // else, assume touch events available
        evt = "touchend" ;                                          // not optimum, but works

    //el = document.getElementById("id_btnHello") ;
    //el.addEventListener(evt, myEventHandler, false) ;
    
    //1. Al cliar un botó, utilitza navigator.notification per fer vibrar el móbil 1 segon.
     el = document.getElementById("vibrate_button") ;
     el.addEventListener(evt, vibrateMobile, false) ;
    
    //2. Al cliar un botó, utilitza navigator.notification per mostrar 2 alertes sonores. 
     el = document.getElementById("sound_button") ;
     el.addEventListener(evt, soundMobile, false) ;
    
    //3. Al clicar un botó utilitza navigator.notification per preguntar un text al usuari. Un cop introduït el text mostra’l amb un navigator.alert. 
     el = document.getElementById("input_button") ;
     el.addEventListener(evt, inputMobile, false) ;
    
    //4. Seleccionar una imatge de la galeria d’imatges i mostrar-la en pantalla. 
     el = document.getElementById("photoLibrary_button") ;
     el.addEventListener(evt, getPicture, false) ;
    
    //5. Realitzar una fotografia amb la càmera web i mostrar-la en pantalla. 
     el = document.getElementById("camera_button") ;
     el.addEventListener(evt, takePicture, false) ;
    
    //6. Al clicar un botó mostra per pantalla amb heading.magneticHeading la desviació del nord en graus.
     el = document.getElementById("bruixola_button") ;
     el.addEventListener(evt, initCompass, false) ;
    
    //7. Al clicar un botó, mostra per pantalla les dades corresponents a l’acceleròmetre y el grau de rotació del mòbil. 
     el = document.getElementById("accelerometer_button") ;
     el.addEventListener(evt, initAccelerometer, false) ;
    
    //8. Al cliar un botó, utilitza navigator.notification per preguntar al usuari quantes alertes sonores vol reproduir, i reprodueix-les. 
     el = document.getElementById("sound_button_page") ;
     el.addEventListener(evt, soundPage, false) ;
    
     el = document.getElementById("sound_alerts_form") ;
     el.addEventListener(evt, soundAlertsForm, false) ;
    
    //9. Al clicar un botó el mòbil vibri tantes vegades com l’usuari indiqui en un formulari (0~5) 
     el = document.getElementById("vibration_button_page") ;
     el.addEventListener(evt, vibrationPage, false) ;
    
     el = document.getElementById("vibration_form") ;
     el.addEventListener(evt, vibrationForm, false) ;
    
    //10. Al clicar un botó en el mòbil soni una alerta tantes vegades com l’usuari indiqui en un formulari (0~5)
     el = document.getElementById("sound_button_page2") ;
     el.addEventListener(evt, soundPage2, false) ;
    
    //11. Permet escollir a l’usuari si vol seleccionar una imatge de la galeria d’imatges o de la càmera web. Mostra-la en pantalla. Si es selecciona una altre imatge, s’ha de mostrar al costat.
     el = document.getElementById("images_button") ;
     el.addEventListener(evt, imagesPage3, false) ;
    
     el = document.getElementById("camera_page3_button") ;
     el.addEventListener(evt, cameraPage3, false) ;
    
     el = document.getElementById("library_page3_button") ;
     el.addEventListener(evt, libraryPage3, false) ;
    
    // NOTE: ...you can put other miscellaneous init stuff in this function...
    // NOTE: ...and add whatever else you want to do now that the app has started...
    // NOTE: ...or create your own init handlers outside of this file that trigger off the "app.Ready" event...

    app.initDebug() ;           // just for debug, not required; keep it if you want it or get rid of it
    app.hideSplashScreen() ;    // after init is good time to remove splash screen; using a splash screen is optional

    // app initialization is done
    // app event handlers are ready
    // exit to idle state and wait for app events...

    app.consoleLog(fName, "exit") ;
} ;
document.addEventListener("app.Ready", app.initEvents, false) ;



// Just a bunch of useful debug console.log() messages.
// Runs after underlying device native code and webview/browser is ready.
// The following is just for debug, not required; keep it if you want or get rid of it.

app.initDebug = function() {
    "use strict" ;
    var fName = "app.initDebug():" ;
    app.consoleLog(fName, "entry") ;

    if( window.device && device.cordova ) {                     // old Cordova 2.x version detection
        app.consoleLog("device.version: " + device.cordova) ;   // print the cordova version string...
        app.consoleLog("device.model: " + device.model) ;
        app.consoleLog("device.platform: " + device.platform) ;
        app.consoleLog("device.version: " + device.version) ;
    }

    if( window.cordova && cordova.version ) {                   // only works in Cordova 3.x
        app.consoleLog("cordova.version: " + cordova.version) ; // print new Cordova 3.x version string...

        if( cordova.require ) {                                 // print included cordova plugins
            app.consoleLog(JSON.stringify(cordova.require('cordova/plugin_list').metadata, null, 1)) ;
        }
    }

    app.consoleLog(fName, "exit") ;
} ;



// Using a splash screen is optional. This function will not fail if none is present.
// This is also a simple study in the art of multi-platform device API detection.

app.hideSplashScreen = function() {
    "use strict" ;
    var fName = "app.hideSplashScreen():" ;
    app.consoleLog(fName, "entry") ;

    // see https://github.com/01org/appframework/blob/master/documentation/detail/%24.ui.launch.md
    // Do the following if you disabled App Framework autolaunch (in index.html, for example)
    // $.ui.launch() ;

    if( navigator.splashscreen && navigator.splashscreen.hide ) {   // Cordova API detected
        navigator.splashscreen.hide() ;
    }
    if( window.intel && intel.xdk && intel.xdk.device ) {           // Intel XDK device API detected, but...
        if( intel.xdk.device.hideSplashScreen )                     // ...hideSplashScreen() is inside the base plugin
            intel.xdk.device.hideSplashScreen() ;
    }

    app.consoleLog(fName, "exit") ;
} ;
