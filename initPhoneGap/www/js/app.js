/*
 * Copyright © 2012-2015, Intel Corporation. All rights reserved.
 * Please see the included README.md file for license terms and conditions.
 */


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false app:false, dev:false, cordova:false */



// This file contains your event handlers, the center of your application.
// NOTE: see app.initEvents() in init-app.js for event handler initialization code.

// function myEventHandler() {
//     "use strict" ;
// // ...event handler code here...
// }


// ...additional event handlers here...

//1. Al cliar un botó, utilitza navigator.notification per fer vibrar el móbil 1 segon.
function vibrateMobile(evt) {
    navigator.notification.vibrate(500);
    alert("Vibrant!");
}

//2. Al cliar un botó, utilitza navigator.notification per mostrar 2 alertes sonores. 
function  soundMobile(evt) {
    navigator.notification.beep(2);
    alert("Sonant!");
}

//3. Al clicar un botó utilitza navigator.notification per preguntar un text al usuari. Un cop introduït el text mostra’l amb un navigator.alert. 
function inputMobile(evt) {
    navigator.notification.prompt(
        'Com et dius?',            // message
        rebreResposta,             // callback to invoke
        'Input Text',              // title
        ['Fet!','Cancel·la'],      // buttonLabels
        'Introdueix el teu nom'    // defaultText
    );
}

function rebreResposta(results) {
    alert("Hola " + results.input1 + "!");
}

//4. Seleccionar una imatge de la galeria d’imatges i mostrar-la en pantalla. 
function getPicture(evt) {
    navigator.camera.getPicture(getPictureSuccess, onFail, {
    destinationType: navigator.camera.DestinationType.FILE_URI, 
    sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY});
}

function getPictureSuccess(imageURI) {
    var nImg = document.getElementById("imageView");
    nImg.src = imageURI;
}

//5. Realitzar una fotografia amb la càmera web i mostrar-la en pantalla. 
function takePicture(evt) {
    navigator.camera.getPicture(takePictureSuccess, onFail, {quality: 100,
    destinationType: navigator.camera.DestinationType.DATA_URL,
    sourceType: navigator.camera.PictureSourceType.CAMERA});
}

function takePictureSuccess(imageData) {
    var nImg = document.getElementById("imageView");
    nImg.src = "data:image/jpeg;base64," + imageData;
}

function onFail() {
    alert("It's impossible to show the image...");
}
    
//6. Al clicar un botó mostra per pantalla amb heading.magneticHeading la desviació del nord en graus.
function initCompass() {
    var options = { frequency: 1000 };
    var watchID = navigator.compass.watchHeading(showCompass, onError, options);
}

function showCompass(heading){
    var deg = heading.magneticHeading;
    alert("position:" + deg);
}

function onError() {
    alert("It's impossible to use the Compass at this moment...");
}

//7. Al clicar un botó, mostra per pantalla les dades corresponents a l’acceleròmetre y el grau de rotació del mòbil. 
function initAccelerometer() {
    var options = {frequency: 1000};
    var accelID = navigator.accelerometer.watchAcceleration(infoAcceleration, onErrorAccelerometer, options);
}

function infoAcceleration(acceleration) {
    alert("X: " + acceleration.x + "; Y: " + acceleration.y + "; Z: " + acceleration.z);
}

function onErrorAccelerometer() {
    alert("It's impossible to use the Accelerometer at this moment...");
} 

//8. Al cliar un botó, utilitza navigator.notification per preguntar al usuari quantes alertes sonores vol reproduir, i reprodueix-les. 
function soundPage(evt) {
    $.mobile.changePage($("#page1"), "none");
    alert("Escolleix les vegades!!");
}
    
function soundAlertsForm(evt) {
    var option = $( "#sound_alerts_form" ).val();
    navigator.notification.beep(option);
    
    alert("Sonant " + option + " vegades!");
}

//9. Al clicar un botó el mòbil vibri tantes vegades com l’usuari indiqui en un formulari (0~5) 
function vibrationPage(evt) {
    $.mobile.changePage($("#page2"), "none");
    alert("Escolleix les vegades!");
}

function vibrationForm(evt) {
    var option = $( "#vibration_form" ).val();
    navigator.notification.vibrate(option * 100);
    
    alert("Vibrant " + option + " vegades!");
}
    
//10. Al clicar un botó en el mòbil soni una alerta tantes vegades com l’usuari indiqui en un formulari (0~5)
function soundPage2(evt) {
    navigator.notification.prompt(
        'Quantes vegades vols que soni?',            // message
        rebreResposta2,             // callback to invoke
        'Escriu un nombre',              // title
        ['Fet!','Cancel·la'],      // buttonLabels
        '1'    // defaultText
    );
}

function rebreResposta2(results) {
    navigator.notification.beep(results.input1);
    alert("Sonant " + results.input1 + " vegades!");
}

//11. Permet escollir a l’usuari si vol seleccionar una imatge de la galeria d’imatges o de la càmera web. Mostra-la en pantalla. Si es selecciona una altre imatge, s’ha de mostrar al costat. 
function imagesPage3(evt) {
    $.mobile.changePage($("#page3"), "none");
    alert("Escolleix diferents fotos!");
}

function libraryPage3(evt) {
    navigator.camera.getPicture(getPictureSuccessPage3, onFail, {
    destinationType: navigator.camera.DestinationType.FILE_URI, 
    sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY});
}

function getPictureSuccessPage3(imageURI) {
    var nImg = document.getElementById("img1_pag3");
    nImg.src = imageURI;
}

function cameraPage3(evt) {
    navigator.camera.getPicture(takePictureSuccessPage3, onFail, {quality: 100,
    destinationType: navigator.camera.DestinationType.DATA_URL,
    sourceType: navigator.camera.PictureSourceType.CAMERA});
}

function takePictureSuccessPage3(imageData) {
    var nImg = document.getElementById("img2_pag3");
    nImg.src = "data:image/jpeg;base64," + imageData;
}
    
    